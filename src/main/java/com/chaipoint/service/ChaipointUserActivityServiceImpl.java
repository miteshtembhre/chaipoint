package com.chaipoint.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chaipoint.entity.FollowingUser;
import com.chaipoint.entity.ShowNewsFeed;
import com.chaipoint.entity.User;
import com.chaipoint.exception.NoFollowingUserFoundException;
import com.chaipoint.exception.NoShowNewsFeedFoundException;
import com.chaipoint.exception.UserNotFoundException;
import com.chaipoint.model.ResponseModel;
import com.chaipoint.repository.FollwingUserRepository;
import com.chaipoint.repository.ShowNewsFeedRepository;
import com.chaipoint.repository.UserRepository;

/**
 * 
 * @author miteshtembhre
 *
 */
@Service
public class ChaipointUserActivityServiceImpl implements ChaipointUserActivityService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ShowNewsFeedRepository showNewsFeedRepository;
	@Autowired
	private FollwingUserRepository follwingUserRepository;

	@Override
	public ResponseModel userPost(String emailId, String post) throws Exception {
		ShowNewsFeed showNewsFeed = null;
		User user = userRepository.findByEmailId(emailId);
		if (user != null) {
			showNewsFeed = new ShowNewsFeed();
			showNewsFeed.setNews(post);
			showNewsFeed.setUserEmailId(emailId);
			showNewsFeed.setCreatedOn(new Date());
			showNewsFeedRepository.saveAndFlush(showNewsFeed);
		} else {
			throw new UserNotFoundException("User Not Found", 400, "NOT FOUND");
		}
		return new ResponseModel("Post has been submitted successfully ...!!!");
	}

	@Override
	public ResponseModel userFollow(String emailId, String followingEmailId) throws Exception {
		FollowingUser followingUser = null;
		User userDeatil = userRepository.findByEmailId(emailId);
		User followingUserDetail = userRepository.findByEmailId(followingEmailId);
		if ((userDeatil != null) && (followingUserDetail != null)) {
			followingUser = follwingUserRepository.findByUserEmailIdAndFollowingUserEmailId(emailId, followingEmailId);
			if (followingUser == null) {
				followingUser = new FollowingUser();
				followingUser.setUserEmailId(emailId);
				followingUser.setFollowingUserEmailId(followingEmailId);
				follwingUserRepository.saveAndFlush(followingUser);
			}
		} else {
			throw new UserNotFoundException("User Not Found", 400, "NOT FOUND");
		}
		return new ResponseModel(
				"You have been following user " + followingUserDetail.getName() + " successfully...!!!");
	}

	@Override
	public ResponseModel postUpvaotes(String userEmailId, String followingUserEmailId, int feedId) throws Exception {
		ShowNewsFeed showNewsFeed = null;
		User user = userRepository.findByEmailId(userEmailId);
		if (user != null) {
			FollowingUser followingUser = follwingUserRepository.findByUserEmailIdAndFollowingUserEmailId(userEmailId,
					followingUserEmailId);
			if (followingUser != null) {
				showNewsFeed = showNewsFeedRepository.findByFeedId(feedId);
				if (showNewsFeed != null) {
					showNewsFeed.setUpvotes(showNewsFeed.getUpvotes() + 1);
					if (showNewsFeed.getDownvotes() != 0)
						showNewsFeed.setDownvotes(showNewsFeed.getDownvotes() - 1);
					showNewsFeedRepository.update(feedId, showNewsFeed.getUpvotes(), showNewsFeed.getDownvotes());
				} else {
					throw new NoFollowingUserFoundException("No ShowNewsFeedFound Exception", 404, "NOT FOUND");
				}
			} else {
				throw new NoFollowingUserFoundException(
						"As you are not following any user, so you are not eligible to devote any post", 404,
						"NOT FOUND");
			}
		} else {
			throw new UserNotFoundException("User Not Found", 404, "NOT FOUND");
		}
		return new ResponseModel("Post with id " + showNewsFeed.getFeedId() + " upvoted...!!!");
	}

	@Override
	public ResponseModel postDownvaotes(String userEmailId, String followingUserEmailId, int feedId) throws Exception {
		ShowNewsFeed showNewsFeed = null;
		User user = userRepository.findByEmailId(userEmailId);
		if (user != null) {
			FollowingUser followingUser = follwingUserRepository.findByUserEmailIdAndFollowingUserEmailId(userEmailId,
					followingUserEmailId);
			if (followingUser != null) {
				showNewsFeed = showNewsFeedRepository.findByFeedId(feedId);
				if (showNewsFeed != null) {
					showNewsFeed.setDownvotes(showNewsFeed.getDownvotes() + 1);
					if (showNewsFeed.getUpvotes() != 0)
						showNewsFeed.setUpvotes(showNewsFeed.getUpvotes() - 1);
					showNewsFeedRepository.update(feedId, showNewsFeed.getUpvotes(), showNewsFeed.getDownvotes());
				} else {
					throw new NoShowNewsFeedFoundException("No ShowNewsFeedFound Exception", 404, "NOT FOUND");
				}
			} else {
				throw new NoFollowingUserFoundException(
						"As you are not following any user, so you are not eligible to downvote any post", 404,
						"NOT FOUND");
			}
		} else {
			throw new UserNotFoundException("User Not Found", 404, "NOT FOUND");
		}
		return new ResponseModel("Post with id " + showNewsFeed.getFeedId() + " downvoted...!!!");
	}

	@Override
	public ResponseModel postComment(String userEmailId, String followingUserEmailId, int feedId, String comment)
			throws Exception {
		ShowNewsFeed showNewsFeed = null;
		User user = userRepository.findByEmailId(userEmailId);
		if (user != null) {
			FollowingUser followingUser = follwingUserRepository.findByUserEmailIdAndFollowingUserEmailId(userEmailId,
					followingUserEmailId);
			if (followingUser != null) {
				showNewsFeed = showNewsFeedRepository.findByFeedId(feedId);
				if (showNewsFeed != null) {
					showNewsFeed.setComment(comment);
					showNewsFeedRepository.updateCommnet(feedId, showNewsFeed.getComment());
				} else {
					throw new NoShowNewsFeedFoundException("No ShowNewsFeedFound Exception", 404, "NOT FOUND");
				}
			} else {
				throw new NoFollowingUserFoundException(
						"As you are not following any user, so you are not eligible to comment on any post", 404,
						"NOT FOUND");
			}
		} else {
			throw new UserNotFoundException("User Not Found", 404, "NOT FOUND");
		}
		return new ResponseModel("Post with id " + showNewsFeed.getFeedId() + " commented...!!!");
	}

	@Override
	public ResponseModel showNewsFeeds(String emailId) throws Exception {
		List<ShowNewsFeed> showNewsFeeds = new ArrayList<>();

		List<FollowingUser> followingUsers = follwingUserRepository.findByUserEmailId(emailId);

		showNewsFeedRepository.findByUserEmailId(emailId).forEach(showNewsFeed -> showNewsFeeds.add(showNewsFeed));

		for (FollowingUser followingUser : followingUsers) {
			showNewsFeedRepository.findByUserEmailId(followingUser.getFollowingUserEmailId())
					.forEach(followingUserShowNewsFeed -> showNewsFeeds.add(followingUserShowNewsFeed));
		}

		if (showNewsFeeds.isEmpty())
			throw new NoShowNewsFeedFoundException("No ShowNewsFeedFound Exception", 404, "NOT FOUND");

		return new ResponseModel(showNewsFeeds);
	}
}
