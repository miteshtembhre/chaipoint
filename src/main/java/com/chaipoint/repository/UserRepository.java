package com.chaipoint.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.chaipoint.entity.User; 

/**
 * 
 * @author miteshtembhre
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Serializable>{
	
	public User findById(int id);

	public User findByEmailId(String emailId);
	
	public User findByEmailIdAndPassword(String emailId, String password);

}
