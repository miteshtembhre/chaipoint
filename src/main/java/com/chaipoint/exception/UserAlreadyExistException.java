package com.chaipoint.exception;

import java.time.LocalDateTime;

/**
 * 
 * @author miteshtembhre
 *
 */
public class UserAlreadyExistException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CustomExceptionResponse customExceptionResponse;

	public CustomExceptionResponse getCustomerExceptionResponse() {
		return customExceptionResponse;
	}

	public UserAlreadyExistException() {
		super();
	}

	public UserAlreadyExistException(String message) {
		super(message);
	}

	public UserAlreadyExistException(String message, Throwable cause) {
		super(message, cause);
	}

	public UserAlreadyExistException(String message, Integer status, String error) {
		super(message);
		customExceptionResponse = new CustomExceptionResponse(LocalDateTime.now(), status, error, message);
	}
}
