package com.chaipoint.exception;

import java.time.LocalDateTime;

/**
 * 
 * @author miteshtembhre
 *
 */
public class NoFollowingUserFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CustomExceptionResponse customExceptionResponse;

	public CustomExceptionResponse getCustomerExceptionResponse() {
		return customExceptionResponse;
	}

	public NoFollowingUserFoundException() {
		super();
	}

	public NoFollowingUserFoundException(String message) {
		super(message);
	}

	public NoFollowingUserFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoFollowingUserFoundException(String message, Integer status, String error) {
		super(message);
		customExceptionResponse = new CustomExceptionResponse(LocalDateTime.now(), status, error, message);
	}
}