package com.chaipoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * @author miteshtembhre
 *
 */
@EnableSwagger2
@SpringBootApplication
@ComponentScan(basePackages = { "com.chaipoint" })
public class ChaiPointApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChaiPointApplication.class, args);
	}

}
