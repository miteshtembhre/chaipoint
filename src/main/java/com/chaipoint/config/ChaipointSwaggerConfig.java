package com.chaipoint.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * 
 * @author miteshtembhre
 *
 */
@Configuration
public class ChaipointSwaggerConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.chaipoint"))
				.paths(PathSelectors.regex("/chaipoint.*")).build().apiInfo(metaInfo());
	}

	
	private ApiInfo metaInfo() {
		@SuppressWarnings("deprecation")
		ApiInfo apiInfo = new ApiInfo("Chaipoint corp API's", 
				"Chaipoint API's for users", 
				"1.0", 
				"Terms of service", 
				"apache lience version 2.0", 
				"https://www.apache.org/license.html", "");
		return apiInfo;
	}
}
