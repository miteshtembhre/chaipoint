This is simple java spring boot application with H2 in memory database.

For running this project you just need to import it any java supportive IDE and then run it as spring boot application.

By default port is 8080 only 

Testing :

Swagger UI : - http://localhost:8080/swagger-ui.html#/chaipoint-controller/postDownVaotesUsingPOST

H2 Console : - http://localhost:8080/h2-console/login.jsp

Username - sa
password - password
JDBC URl : jdbc:h2:mem:chaipoint_db