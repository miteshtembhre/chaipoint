package com.chaipoint.exception;

import java.time.LocalDateTime;

public class NoShowNewsFeedFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CustomExceptionResponse customExceptionResponse;

	public CustomExceptionResponse getCustomerExceptionResponse() {
		return customExceptionResponse;
	}

	public NoShowNewsFeedFoundException() {
		super();
	}

	public NoShowNewsFeedFoundException(String message) {
		super(message);
	}

	public NoShowNewsFeedFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoShowNewsFeedFoundException(String message, Integer status, String error) {
		super(message);
		customExceptionResponse = new CustomExceptionResponse(LocalDateTime.now(), status, error, message);
	}
}