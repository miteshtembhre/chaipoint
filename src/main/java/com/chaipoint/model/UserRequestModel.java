package com.chaipoint.model;

import java.io.Serializable;

/**
 * 
 * @author miteshtembhre
 *
 */
public class UserRequestModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		
	private String name;
	
	private String emailId;
	
	private Long mobileNumber;
	
	private String password;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Long getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(Long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
