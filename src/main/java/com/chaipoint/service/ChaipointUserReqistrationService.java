package com.chaipoint.service;

import com.chaipoint.model.ResponseModel;
import com.chaipoint.model.UserRequestModel;

/**
 * 
 * @author miteshtembhre
 *
 */
public interface ChaipointUserReqistrationService {
	
	public ResponseModel signup(UserRequestModel user) throws Exception;

	public ResponseModel signIn(String emailId, String password) throws Exception;

}
