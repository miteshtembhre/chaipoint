package com.chaipoint.service;

import com.chaipoint.model.ResponseModel;

/**
 * 
 * @author miteshtembhre
 *
 */
public interface ChaipointUserActivityService {

	public ResponseModel userPost(String emailId, String post) throws Exception;

	public ResponseModel userFollow(String emailId, String followingEmailId) throws Exception;

	public ResponseModel postUpvaotes(String userEmailId, String followingUserEmailId, int feedId) throws Exception;

	public ResponseModel postDownvaotes(String userEmailId, String followingUserEmailId, int feedId) throws Exception;

	public ResponseModel showNewsFeeds(String emailId) throws Exception;

	public ResponseModel postComment(String userEmailId, String followingUserEmailId, int feedId, String comment)throws Exception;

}
