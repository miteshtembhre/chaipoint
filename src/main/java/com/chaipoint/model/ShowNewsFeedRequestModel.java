package com.chaipoint.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author miteshtembhre
 *
 */
public class ShowNewsFeedRequestModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int feedId;

	private String news;
	
	private Date createdOn;

	public int getFeedId() {
		return feedId;
	}

	public void setFeedId(int feedId) {
		this.feedId = feedId;
	}

	public String getNews() {
		return news;
	}

	public void setNews(String news) {
		this.news = news;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	
}
