package com.chaipoint.exception;

import java.time.LocalDateTime;

/**
 * 
 * @author miteshtembhre
 *
 */
public class UserNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private CustomExceptionResponse customExceptionResponse;

	public CustomExceptionResponse getCustomerExceptionResponse() {
		return customExceptionResponse;
	}

	public UserNotFoundException() {
		super();
	}

	public UserNotFoundException(String message) {
		super(message);
	}

	public UserNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public UserNotFoundException(String message, Integer status, String error) {
		super(message);
		customExceptionResponse = new CustomExceptionResponse(LocalDateTime.now(), status, error, message);
	}
}
