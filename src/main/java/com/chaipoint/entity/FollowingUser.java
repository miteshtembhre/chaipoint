package com.chaipoint.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author miteshtembhre
 *
 */
@Entity
@Table
public class FollowingUser implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column
	private String userEmailId;
	@Column
	private String followingUserEmailId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserEmailId() {
		return userEmailId;
	}
	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}
	public String getFollowingUserEmailId() {
		return followingUserEmailId;
	}
	public void setFollowingUserEmailId(String followingUserEmailId) {
		this.followingUserEmailId = followingUserEmailId;
	}
}
