package com.chaipoint.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chaipoint.entity.ShowNewsFeed;
import com.chaipoint.entity.User;
import com.chaipoint.exception.UserAlreadyExistException;
import com.chaipoint.exception.UserNotFoundException;
import com.chaipoint.model.ResponseModel;
import com.chaipoint.model.UserRequestModel;
import com.chaipoint.repository.ShowNewsFeedRepository;
import com.chaipoint.repository.UserRepository;

/**
 * 
 * @author miteshtembhre
 *
 */
@Service
public class ChaipointUserRegistrationServiceImpl implements ChaipointUserReqistrationService{
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ShowNewsFeedRepository showNewsFeedRepository;

	@Override
	public ResponseModel signup(UserRequestModel userRequest) throws Exception {
		User user = null;
		if (userRepository.findByEmailId(userRequest.getEmailId()) == null) {
			user = new User();
			user.setEmailId(userRequest.getEmailId());
			user.setMobileNumber(userRequest.getMobileNumber());
			user.setName(userRequest.getName());
			user.setPassword(userRequest.getPassword());
			userRepository.saveAndFlush(user);
		} else {
			throw new UserAlreadyExistException("User already exist", 422, "UNPROCESSABLE_ENTITY");
		}
		return new ResponseModel("User " + user.getName() + " has been signup successfully...!!!");
	}

	@Override
	public ResponseModel signIn(String emailId, String password) throws Exception {
		List<ShowNewsFeed> showNewsFeeds = null;
		User validUser = userRepository.findByEmailIdAndPassword(emailId, password);
		if (validUser != null)
			showNewsFeeds = showNewsFeedRepository.findByUserEmailId(emailId);
		else
			throw new UserNotFoundException("User Not Found", 400, "NOT FOUND");

		if (showNewsFeeds.isEmpty()) {
			return new ResponseModel("No Post to show...!!!");
		}
		return new ResponseModel(showNewsFeeds);
	}

}
