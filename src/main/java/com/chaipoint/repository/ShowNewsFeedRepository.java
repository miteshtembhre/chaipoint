package com.chaipoint.repository;

import java.io.Serializable;
import java.util.List;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.chaipoint.entity.ShowNewsFeed;

/**
 * 
 * @author miteshtembhre
 *
 */
@Repository
public interface ShowNewsFeedRepository extends JpaRepository<ShowNewsFeed, Serializable>{
	
	public List<ShowNewsFeed> findByUserEmailId(String userEmailId);
	
	public ShowNewsFeed findByFeedId(int feedId);
	
	
	
	@Modifying(flushAutomatically = true, clearAutomatically = true)
	@Transactional
	@Query(value = "UPDATE ShowNewsFeed a SET a.upvotes =:upvotes , a.downvotes =:downvotes WHERE a.feedId =:feedId")
    int update(@Param("feedId") int feedId, @Param("upvotes") int upvotes , @Param("downvotes") int downvotes);
	
	@Modifying(flushAutomatically = true, clearAutomatically = true)
	@Transactional
	@Query(value = "UPDATE ShowNewsFeed a SET a.comment =:comment WHERE a.feedId =:feedId")
    int updateCommnet(@Param("feedId") int feedId, @Param("comment") String comment);
}
