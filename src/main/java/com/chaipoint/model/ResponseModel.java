package com.chaipoint.model;

import java.io.Serializable;
import java.util.List;

import com.chaipoint.entity.ShowNewsFeed;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author miteshtembhre
 *
 */
@JsonInclude(Include.NON_NULL)
public class ResponseModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String message;
	private List<ShowNewsFeed> showNewsFeeds;
	
	public ResponseModel(String message) {
		this.setMessage(message);
	}
	
	public ResponseModel(List<ShowNewsFeed> showNewsFeeds) {
		this.showNewsFeeds=showNewsFeeds;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<ShowNewsFeed> getShowNewsFeeds() {
		return showNewsFeeds;
	}

	public void setShowNewsFeeds(List<ShowNewsFeed> showNewsFeeds) {
		this.showNewsFeeds = showNewsFeeds;
	}

}
