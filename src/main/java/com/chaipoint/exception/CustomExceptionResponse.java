package com.chaipoint.exception;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 
 * @author miteshtembhre
 *
 */
public class CustomExceptionResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
	private LocalDateTime timeDateTime;
	private Integer status;
	private String error;
	private String message;

	public CustomExceptionResponse(LocalDateTime timeDateTime, Integer status, String error, String message) {
		this.timeDateTime = timeDateTime;
		this.status = status;
		this.error = error;
		this.message = message;
	}

	public LocalDateTime getTimeDateTime() {
		return timeDateTime;
	}

	public void setTimeDateTime(LocalDateTime timeDateTime) {
		this.timeDateTime = timeDateTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
