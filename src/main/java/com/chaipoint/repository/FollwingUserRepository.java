package com.chaipoint.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.chaipoint.entity.FollowingUser;

/**
 * 
 * @author miteshtembhre
 *
 */
@Repository
public interface FollwingUserRepository extends JpaRepository<FollowingUser, Serializable> {

	public List<FollowingUser> findByUserEmailId(String userEmailId);

	public FollowingUser findByUserEmailIdAndFollowingUserEmailId(String userEmailId, String followingUserEmailId);
}
