package com.chaipoint.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chaipoint.exception.NoFollowingUserFoundException;
import com.chaipoint.exception.NoShowNewsFeedFoundException;
import com.chaipoint.exception.UserNotFoundException;
import com.chaipoint.model.ResponseModel;
import com.chaipoint.service.ChaipointUserActivityService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
/**
 * 
 * @author miteshtembhre
 *
 */
@RestController
@RequestMapping("/chaipoint/user/")
@Api(value = "Chaipoint API's for user to signup, signin and post etc..")
public class ChaipointUserActivityController {

	@Autowired
	private ChaipointUserActivityService chaipointService;

	@ApiOperation(value = "This api can be used to post anything by valid user ...!!!")
	@PostMapping("post/{emailId}/{post}")
	public ResponseEntity<Object> userPost(@PathVariable String emailId, @PathVariable String post) {
		ResponseModel responseModel = null;
		try {
			responseModel = chaipointService.userPost(emailId, post);
			return new ResponseEntity<Object>(responseModel, HttpStatus.OK);
		} catch (UserNotFoundException e) {
			return new ResponseEntity<Object>(e.getCustomerExceptionResponse(), HttpStatus.UNPROCESSABLE_ENTITY);
		} catch (Exception e) {
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@ApiOperation(value = "This api can be used to fetch all the shownewsfeed posted by user ...!!!")
	@PostMapping("shownewsfeeds/{emailId}")
	public ResponseEntity<Object> showNewsFeeds(@PathVariable String emailId) {
		ResponseModel responseModel = null;
		try {
			responseModel = chaipointService.showNewsFeeds(emailId);
			return new ResponseEntity<Object>(responseModel, HttpStatus.OK);
		} catch (NoShowNewsFeedFoundException e) {
			return new ResponseEntity<Object>(e.getCustomerExceptionResponse(), HttpStatus.UNPROCESSABLE_ENTITY);
		} catch (Exception e) {
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@ApiOperation(value = "This api can be used to follow any other user...!!!")
	@PostMapping("follow/{emailId}/{followingEmailId}")
	public ResponseEntity<Object> userFollow(@PathVariable String emailId, @PathVariable String followingEmailId) {
		ResponseModel responseModel = null;
		try {
			responseModel = chaipointService.userFollow(emailId, followingEmailId);
			return new ResponseEntity<Object>(responseModel, HttpStatus.OK);
		} catch (UserNotFoundException e) {
			return new ResponseEntity<Object>(e.getCustomerExceptionResponse(), HttpStatus.UNPROCESSABLE_ENTITY);
		} catch (Exception e) {
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@ApiOperation(value = "This api can be used to upvotes any other user post...!!!")
	@PostMapping("post/upvotes/{userEmailId}/{followingUserEmailId}/{feedId}")
	public ResponseEntity<Object> postUpvaotes(@PathVariable String userEmailId,
			@PathVariable String followingUserEmailId, @PathVariable int feedId) {
		ResponseModel responseModel = null;
		try {
			responseModel = chaipointService.postUpvaotes(userEmailId, followingUserEmailId, feedId);
			return new ResponseEntity<Object>(responseModel, HttpStatus.OK);
		} catch (UserNotFoundException e) {
			return new ResponseEntity<Object>(e.getCustomerExceptionResponse(), HttpStatus.UNPROCESSABLE_ENTITY);
		} catch (NoShowNewsFeedFoundException e) {
			return new ResponseEntity<Object>(e.getCustomerExceptionResponse(), HttpStatus.UNPROCESSABLE_ENTITY);
		} catch (NoFollowingUserFoundException e) {
			return new ResponseEntity<Object>(e.getCustomerExceptionResponse(), HttpStatus.UNPROCESSABLE_ENTITY);
		} catch (Exception e) {
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@ApiOperation(value = "This api can be used to downvotes any other user post...!!!")
	@PostMapping("post/downvotes/{userEmailId}/{followingUserEmailId}/{feedId}")
	public ResponseEntity<Object> postDownVaotes(@PathVariable String userEmailId,
			@PathVariable String followingUserEmailId, @PathVariable int feedId) {
		ResponseModel responseModel = null;
		try {
			responseModel = chaipointService.postDownvaotes(userEmailId, followingUserEmailId, feedId);
			return new ResponseEntity<Object>(responseModel, HttpStatus.OK);
		} catch (UserNotFoundException e) {
			return new ResponseEntity<Object>(e.getCustomerExceptionResponse(), HttpStatus.UNPROCESSABLE_ENTITY);
		} catch (NoShowNewsFeedFoundException e) {
			return new ResponseEntity<Object>(e.getCustomerExceptionResponse(), HttpStatus.UNPROCESSABLE_ENTITY);
		} catch (NoFollowingUserFoundException e) {
			return new ResponseEntity<Object>(e.getCustomerExceptionResponse(), HttpStatus.UNPROCESSABLE_ENTITY);
		} catch (Exception e) {
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@ApiOperation(value = "This api can be used to comment on post...!!!")
	@PostMapping("post/comment/{userEmailId}/{followingUserEmailId}/{feedId}/{comment}")
	public ResponseEntity<Object> postComment(@PathVariable String userEmailId,
			@PathVariable String followingUserEmailId, @PathVariable int feedId, @PathVariable String comment) {
		ResponseModel responseModel = null;
		try {
			responseModel = chaipointService.postComment(userEmailId, followingUserEmailId, feedId, comment);
			return new ResponseEntity<Object>(responseModel, HttpStatus.OK);
		} catch (UserNotFoundException e) {
			return new ResponseEntity<Object>(e.getCustomerExceptionResponse(), HttpStatus.UNPROCESSABLE_ENTITY);
		} catch (NoShowNewsFeedFoundException e) {
			return new ResponseEntity<Object>(e.getCustomerExceptionResponse(), HttpStatus.UNPROCESSABLE_ENTITY);
		} catch (NoFollowingUserFoundException e) {
			return new ResponseEntity<Object>(e.getCustomerExceptionResponse(), HttpStatus.UNPROCESSABLE_ENTITY);
		} catch (Exception e) {
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

}
