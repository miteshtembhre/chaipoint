package com.chaipoint.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chaipoint.exception.UserAlreadyExistException;
import com.chaipoint.exception.UserNotFoundException;
import com.chaipoint.model.ResponseModel;
import com.chaipoint.model.UserRequestModel;
import com.chaipoint.service.ChaipointUserReqistrationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author miteshtembhre
 *
 */
@RestController
@RequestMapping("/chaipoint/user/")
@Api(value = "Chaipoint API's for user to signup and signin etc..")
public class ChaipointUserRegistrationController {
	
	@Autowired
	private ChaipointUserReqistrationService chaipointUserReqistrationService;


	@ApiOperation(value = "This api can be used to signup, through which user can create a profile with certain data ...!!!")
	@PostMapping("signup")
	public ResponseEntity<Object> userSignUp(@RequestBody UserRequestModel user) {
		ResponseModel responseModel = null;
		try {
			responseModel = chaipointUserReqistrationService.signup(user);
			return new ResponseEntity<Object>(responseModel, HttpStatus.OK);
		} catch (UserAlreadyExistException e) {
			return new ResponseEntity<Object>(e.getCustomerExceptionResponse(), HttpStatus.UNPROCESSABLE_ENTITY);
		} catch (Exception e) {
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@ApiOperation(value = "This api can be used to signin in existing user profile with valid username, password...!!!")
	@PostMapping("signin/{emailId}/{password}")
	public ResponseEntity<Object> userSignIn(@PathVariable String emailId, @PathVariable String password) {
		ResponseModel responseModel = null;
		try {
			responseModel = chaipointUserReqistrationService.signIn(emailId, password);
			return new ResponseEntity<Object>(responseModel, HttpStatus.OK);
		} catch (UserNotFoundException e) {
			return new ResponseEntity<Object>(e.getCustomerExceptionResponse(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
